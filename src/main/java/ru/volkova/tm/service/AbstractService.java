package ru.volkova.tm.service;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.AbstractEntity;
import ru.volkova.tm.exception.empty.EmptyIdException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E add(final E entity) {
        if (entity == null) throw new ObjectNotFoundException();
        return repository.add(entity);
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public E removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new ObjectNotFoundException();
        repository.remove(entity);
    }

}
