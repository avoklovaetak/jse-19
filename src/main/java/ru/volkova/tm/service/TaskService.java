package ru.volkova.tm.service;

import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.ITaskService;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyIdException;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.system.IndexIncorrectException;
import ru.volkova.tm.entity.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task startTaskByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task startTaskByIndex(Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task finishTaskById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task finishTaskByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task finishTaskByIndex(Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task changeTaskStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setDateStart(new Date());
        if (status == Status.COMPLETE) task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task changeTaskStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setDateStart(new Date());
        if (status == Status.COMPLETE) task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setDateStart(new Date());
        if (status == Status.COMPLETE) task.setDateFinish(new Date());
        return task;
    }

}
