package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.exception.entity.UserNotFoundException;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (final User user: entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user: entities) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public void removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

}
