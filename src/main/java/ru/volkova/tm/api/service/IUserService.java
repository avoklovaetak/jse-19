package ru.volkova.tm.api.service;

import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;


public interface IUserService extends IService<User> {

    boolean isLoginExists(String login);

    User findByLogin(String login);

    boolean isEmailExists(String email);

    void removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(
            final String userId,
            final String firstName,
            final String secondName,
            final String middleName
    );

}
