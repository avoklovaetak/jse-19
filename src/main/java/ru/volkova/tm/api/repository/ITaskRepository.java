package ru.volkova.tm.api.repository;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

    void removeAllByProjectId(String projectId);

    Task bindTaskByProjectId(String projectId, String taskId);

    Task unbindTaskByProjectId(String projectId, String taskId);

    List<Task> findAll(Comparator<Task> comparator);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
