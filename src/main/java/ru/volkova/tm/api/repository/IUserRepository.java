package ru.volkova.tm.api.repository;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    void removeByLogin(String login);

}
